---
title: "Über uns"
date: 2023-03-01T11:04:49+08:00
draft: false
description: "Über evotrike"

toc:
  auto: true

---

**Safe and sustainable mobility for everyone**

## Mission

- **Safe**: We develop and spread self-driving, slow e-tricycles, so called evotrikes (evolutionary trikes)
- **Sustainable**: We integrate them well in existing public transport- and booking-systems
- **For everyone**: We make them shareable

### Polite, friendly citizen

- The evotrike is limited to a max. speed of 10 km/h and will stop in any case of danger or uncertain situation, e.g. when a person is approaching too close from any side towards the vehicle. 
- Evotrikes play well with other existing ways of transport to enable a sustainable, multi-modal future of transportation. 
- It will always drive on the side of the street, so faster road user like bicycles can take over easily.
- There will be certain roads, which will not be used by the evotrike, e.g. bus-lanes for fast public-transport or corridors for emergency cars. 
- Evotrikes will only park in designated areas, preferable ones which provide a charging possibility.

### Integrated booking system

- The booking-system should be integrated in other trip-planners, so e.g. an evotrike will be already waiting for a customer when leaving the train.

### Privacy-aware sharing

- All sharable evotrikes will be registered in a distributed network to show their location and availability
- If an evotrike is booked by a customer, only the owner the specific bike will receive detailed information about the customer, only as much data which is required for legal reasons and to process the payment. The network itself will not receive any data, but just about the availability-status of this vehicle.
- The network encourages small, private owner to share their evotrikes to the whole community, but there will be also larger, commercial service-providers

### Full community-ownership

- All hard- and software-development is licensed by an AGPL-license, so instead of creating a patents and monopolies the organization is striving to improve the wisdom and way of living for all people on this planet
- A community-owned foundation pushes forward the development and improvement of the evotrike
- There can be commercial operators of evobikes though, which should support the foundation